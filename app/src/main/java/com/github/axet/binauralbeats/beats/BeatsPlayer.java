package com.github.axet.binauralbeats.beats;

import android.content.Context;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.media.SoundPool;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import com.github.axet.androidlibrary.sound.FadeVolume;
import com.github.axet.binauralbeats.R;
import com.github.axet.binauralbeats.activities.MainActivity;
import com.github.axet.binauralbeats.app.Sound;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;

public class BeatsPlayer extends VoicesPlayer {
    private static final String TAG = BeatsPlayer.class.getSimpleName();

    public enum eState {INIT, START, RUNNING, END}

    public static final int MAX_STREAMS = 5;
    public static final float DEFAULT_BG_VOL = 0.7f;
    public static final float DEFAULT_VOLUME = 0.6f;
    public static final float BG_VOLUME_RATIO = 0.4f;

    public static String formatTimeNumberwithLeadingZero(int t) {
        if (t > 9)
            return String.format("%2d", t);
        else
            return String.format("0%1d", t);
    }

    public static long _getClock() {
        return SystemClock.elapsedRealtime();
    }

    public static class PresetsPool {
        Context context;
        Handler handler;
        public SoundPool mSoundPool;
        public Vector<StreamVoice> playingStreams;
        public Set<Integer> loading = new TreeSet<>(); // ready when empty
        public boolean delayed = false; // start when ready

        public int soundUnity;

        public PresetsPool(Context context, Handler handler) {
            this.context = context;
            this.handler = handler;
            playingStreams = new Vector<>(MAX_STREAMS);
            mSoundPool = new SoundPool(MAX_STREAMS, Sound.DEFAULT_STREAM, 0);

            soundUnity = load(R.raw.unity);

            mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
                @Override
                public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                    loading.remove(sampleId);
                    if (loading.isEmpty() && delayed) {
                        PresetsPool.this.onLoadComplete();
                    }
                }
            });
        }

        public int load(int raw) {
            int id = mSoundPool.load(context, raw, 1);
            loading.add(id);
            return id;
        }

        public void add(StreamVoice v) {
            playingStreams.add(v);
            while (playingStreams.size() > MAX_STREAMS) {
                StreamVoice vv = playingStreams.remove(0);
                autoStop(vv);
            }
        }

        public void stop() {
            for (StreamVoice v : playingStreams) {
                autoStop(v);
            }
            playingStreams.clear();
        }

        void autoPause() {
            mSoundPool.autoPause();
            for (StreamVoice v : playingStreams) {
                if (v.fadeOut != null) {
                    handler.removeCallbacks(v.fadeOut); // do not null
                }
            }
        }

        void autoStop(StreamVoice v) {
            mSoundPool.stop(v.streamID);
            if (v.fadeIn != null) {
                handler.removeCallbacks(v.fadeIn);
            }
            if (v.fadeOut != null) {
                handler.removeCallbacks(v.fadeOut); // do not null
            }
        }

        boolean softStop() {
            boolean alive = false;
            for (StreamVoice v : new Vector<>(playingStreams)) {
                if (v.fadeOut == null) {
                    autoStop(v);
                    playingStreams.remove(v);
                } else {
                    alive = true;
                }
            }
            return alive;
        }

        public void release() {
            if (mSoundPool != null) {
                mSoundPool.release();
                mSoundPool = null;
            }
        }

        public void closeSounds() {
            delayed = false;
            for (StreamVoice sound : playingStreams) {
                autoStop(sound);
            }
            playingStreams.clear();
        }

        public void stop(StreamVoice s) {
            autoStop(s);
            playingStreams.remove(s);
        }

        public void onLoadComplete() {
        }
    }

    class RunProgram implements Runnable {
        private static final long TIMER_FSM_DELAY = 1000 / 20;

        private static final int GRAPH_VOICE_VIEW_PAST = 60;
        private static final int GRAPH_VOICE_SPAN = 600;
        private static final int GRAPH_VOICE_UPDATE = 5;

        private Program pR;
        private Period periodsStart;
        private Iterator<Period> periodsIterator;
        private Period currentPeriod;
        private Period nextPeriod;
        private Period prevPeriod;
        private long cT; // current Period start time
        private long startTime;
        private long programLength;
        private String sProgramLength;
        private Handler h;

        public RunProgram(Program pR, Handler h) {
            this.pR = pR;
            this.h = h;

            programLength = pR.getLength();
            sProgramLength = context.getString(R.string.time_format,
                    formatTimeNumberwithLeadingZero((int) programLength / 60),
                    formatTimeNumberwithLeadingZero((int) programLength % 60));
            startTime = _getClock();
            _last_graph_update = 0;
        }

        public void stopProgram() { // soft end, do not close timed sounds, only infinite ones
            playing = false;
            endPeriod();
            softStopVoices();
            softStopBackground();
            h.removeCallbacks(this);
            rp = null;
            presets.delayed = false;
            softEnd();
        }

        private void startPeriod(Period p, boolean start, boolean end, boolean pstart, boolean pend) {
            if (p.voices != null)
                playVoices(p.length * 1000, p.voices, start, end);
            playBackgroundSample(p.length * 1000, p.background, p.getBackgroundvol(), start, end, pstart, pend);
            Log.v(TAG, String.format("New Period - duration %d", p.length));
        }

        void updateStatus(long now, Period p) {
            long delta = (now - startTime) / 1000; // Do not refresh too often
            if (p.voices != null) {
                status = context.getString(R.string.info_timing,
                        freq,
                        formatTimeNumberwithLeadingZero((int) delta / 60),
                        formatTimeNumberwithLeadingZero((int) delta % 60)) + sProgramLength;
            } else {
                status = String.format("%s:%s/",
                        formatTimeNumberwithLeadingZero((int) delta / 60),
                        formatTimeNumberwithLeadingZero((int) delta % 60)) + sProgramLength;
            }
            updatePeriodGraph(delta);
        }

        private void endPeriod() {
        }

        public void catchUpAfterPause(long delta) {
            startTime += delta;
            cT += delta;
        }

        public void run() {
            long now = _getClock();

            switch (s) {
                case START:
                    s = eState.RUNNING;
                    periodsIterator = pR.getPeriodsIterator();
                    cT = now;
                    nextPeriod = periodsIterator.next();
                    periodsStart = nextPeriod;
                    nextPeriod();
                    break;
                case RUNNING:
                    if (!isPlaying())
                        break;
                    float pos = (now - cT) / 1000f;
                    if (pos > currentPeriod.length) { // Current period is over
                        endPeriod();
                        if (nextPeriod == null) { // Finished
                            s = eState.END;
                            updateStatus(now, currentPeriod);
                        } else { // this is a new period
                            cT = now;
                            nextPeriod();
                        }
                    } else {
                        updateStatus(now, currentPeriod);
                    }
                    break;
                case END:
                    stopProgram();
                    return;
            }

            h.postDelayed(this, TIMER_FSM_DELAY);
        }

        private void nextPeriod() {
            prevPeriod = currentPeriod;
            currentPeriod = nextPeriod;
            boolean n = periodsIterator.hasNext();
            if (n)
                nextPeriod = periodsIterator.next();
            else
                nextPeriod = null;
            boolean s = true; // we should fade in by default if we start
            if (prevPeriod != null) // then we in the middle
                s = prevPeriod.background != currentPeriod.background; // we should fade in if background's are differ
            boolean e = true; // we should fade out by default
            if (nextPeriod != null) { // do we have next?
                e = currentPeriod.background != nextPeriod.background; // we should fade if background's are differ
            }
            startPeriod(currentPeriod, currentPeriod == periodsStart, nextPeriod == null, s, e);
        }

        private void updatePeriodGraph(long delta) {
            if (delta >= _last_graph_update + GRAPH_VOICE_UPDATE) {
                viewstart = 0;
                _last_graph_update = delta;
                if (GRAPH_VOICE_SPAN < programLength)
                    viewstart = (int) Math.max(0, delta - GRAPH_VOICE_VIEW_PAST);
                viewsize = GRAPH_VOICE_SPAN;
            }
        }
    }

    private StreamVoice playingBackground;
    private WhiteNoise soundWhiteNoise;
    public PresetsPool presets;
    float mSoundBGVolume = DEFAULT_VOLUME * BG_VOLUME_RATIO;
    float mSoundVcVolume = DEFAULT_VOLUME;
    Program pr;
    RunProgram rp;
    Handler handler;
    String status = "";
    int viewstart;
    int viewsize;
    private long _last_graph_update; // seconds
    long pause;
    float currentPeriodBgVol; // current period background volume
    private eState s = eState.INIT;
    Runnable softEnd = new Runnable() {
        @Override
        public void run() {
            softEnd();
        }
    };

    public BeatsPlayer(final Context context, Program pr, Handler handler, PresetsPool old) {
        super(context);
        this.handler = handler;
        this.pr = pr;
        this.presets = old;

        if (presets == null) {
            presets = new PresetsPool(context, handler) {
                @Override
                public void onLoadComplete() {
                    runProgram();
                }
            };
        }
    }

    public Program getProgram() {
        return pr;
    }

    private void playBackgroundSample(long dur, SoundLoop background, float vol, boolean start, boolean end, boolean pstart, boolean pend) {
        int fadeStart = start ? FADE_PROGRAM : (pstart ? FADE_PERIOD : 0);
        int fadeEnd = end ? FADE_PROGRAM : (pend ? FADE_PERIOD : 0);
        currentPeriodBgVol = vol;
        switch (background) {
            case WHITE_NOISE:
                if (playingBackground != null) {
                    presets.stop(playingBackground);
                    playingBackground = null;
                }
                if (soundWhiteNoise == null) {
                    soundWhiteNoise = new WhiteNoise(context);
                }
                soundWhiteNoise.play(fadeEnd == 0 ? -1 : dur, vol * mSoundBGVolume, fadeStart, fadeEnd);
                soundWhiteNoise.start();
                break;
            case UNITY:
                if (soundWhiteNoise != null) {
                    soundWhiteNoise.close();
                    soundWhiteNoise = null;
                }
                if (playingBackground != null && playingBackground.soundID == presets.soundUnity)
                    update(playingBackground, fadeStart, fadeEnd, currentPeriodBgVol * mSoundBGVolume);
                else
                    playingBackground = play(presets.soundUnity, vol, fadeStart, fadeEnd, currentPeriodBgVol * mSoundBGVolume);
                break;
            case NONE:
                stopBackgroundSample();
                break;
        }
    }

    void update(final StreamVoice v, int fadeStart, int fadeEnd, final float currentVol) {
        final PresetsPool presets = this.presets; // in case of player close, keep update volume
        if (fadeStart > 0) {
            v.fadeIn = new FadeVolume(handler, fadeStart) {
                @Override
                public boolean step(float vol) {
                    presets.mSoundPool.setVolume(v.streamID, vol * currentVol, vol * currentVol);
                    return true;
                }

                @Override
                public void done() {
                    v.fadeIn = null;
                }
            };
            v.fadeIn.run();
        } else {
            presets.mSoundPool.setVolume(v.streamID, currentVol, currentVol);
        }
        if (fadeEnd > 0) {
            v.fadeOut = new FadeVolume(handler, fadeEnd) {
                @Override
                public boolean step(float vol) {
                    vol = (1f - vol);
                    presets.mSoundPool.setVolume(v.streamID, vol * currentVol, vol * currentVol);
                    return true;
                }

                @Override
                public void done() {
                    v.fadeOut = null;
                }
            };
            fadeOutUpdate(v);
        }
    }

    public StreamVoice play(int soundID, float vol, int fadeStart, int fadeEnd, float currentVol) {
        int id = presets.mSoundPool.play(soundID, 0, 0, 0, -1, 1f);
        final StreamVoice v = new StreamVoice(soundID, id, vol, vol, -1, 1f);

        presets.add(v);

        update(v, fadeStart, fadeEnd, currentVol);

        return v;
    }

    void autoResume() {
        presets.mSoundPool.autoResume();
        for (StreamVoice v : presets.playingStreams) {
            if (v.fadeOut != null)
                fadeOutUpdate(v);
        }
    }

    void fadeOutUpdate(StreamVoice v) {
        handler.removeCallbacks(v.fadeOut);
        handler.postDelayed(v.fadeOut, rp.currentPeriod.getLength() * 1000 - (_getClock() - rp.cT) - v.fadeOut.dur);
    }

    private void stopBackgroundSample() {
        if (playingBackground != null) {
            presets.stop(playingBackground);
            playingBackground = null;
        }

        if (presets != null)
            presets.stop();

        if (soundWhiteNoise != null) {
            soundWhiteNoise.close();
            soundWhiteNoise = null;
        }
    }

    boolean softStopVoices() {
        synchronized (lock) {
            if (state.voices == null || state.isInfinite()) {
                stopVoices();
                return false; // alive
            }
        }
        return !state.complete(); // alive
    }

    boolean softStopBackground() {
        boolean alive = false;
        if (soundWhiteNoise != null) {
            synchronized (soundWhiteNoise.lock) {
                if (soundWhiteNoise.state.isInfinite()) {
                    soundWhiteNoise.close();
                    soundWhiteNoise = null;
                } else {
                    alive = !soundWhiteNoise.state.complete();
                }
            }
        }
        if (playingBackground != null) {
            if (playingBackground.fadeOut == null) {
                presets.stop(playingBackground);
                playingBackground = null;
            } else {
                alive = true;
            }
        }
        if (presets.softStop())
            alive = true;
        return alive;
    }

    public boolean isPlaying() {
        if (playing)
            return true;
        if (pause > 0)
            return false;
        if (rp != null) // started but not yet marked as playing
            return true;
        if (presets.delayed) // started but delayed because of SoundPool
            return true;
        return false;
    }

    public void pause() {
        if (soundWhiteNoise != null) {
            soundWhiteNoise.pause();
        }
        playing = false;
        presets.delayed = false; // prevent start playing when sounds ready
        pause = _getClock();
        handler.removeCallbacks(rp);
        presets.autoPause();
        track.pause();
        onPause();
    }

    void closeSounds() {
        stopBackgroundSample();
        handler.removeCallbacks(rp);
        if (presets != null)
            presets.closeSounds();
        stopVoices();
        handler.removeCallbacks(softEnd);
    }

    void resetAllVolumes() {
        for (StreamVoice v : presets.playingStreams) {
            if (v == playingBackground)
                presets.mSoundPool.setVolume(v.streamID, v.leftVol * mSoundBGVolume, v.rightVol * mSoundBGVolume);
            else
                presets.mSoundPool.setVolume(v.streamID, v.leftVol * mSoundVcVolume, v.rightVol * mSoundVcVolume);
        }
        setVolume(mSoundVcVolume);
        if (soundWhiteNoise != null)
            soundWhiteNoise.setVolume(currentPeriodBgVol * mSoundBGVolume);
    }

    public void release() {
        closeSounds();
        shutdown();
        if (presets != null) {
            presets.release();
            presets = null;
        }
    }

    public long getCurrentPosition() {
        return _last_graph_update;
    }

    public String getStatus() {
        return status;
    }

    public int getViewStart() {
        return viewstart;
    }

    public int getViewSize() {
        return viewsize;
    }

    public void play() {
        if (rp != null) { // pause mode
            playing = true;

            long now = _getClock();
            long d = now - pause;
            rp.catchUpAfterPause(d);
            pause = 0;

            autoResume();
            track.play();
            rp.run();
            if (soundWhiteNoise != null)
                soundWhiteNoise.start();

            synchronized (lock) {
                if (moreSamples(state)) {
                    lock.notifyAll();
                }
            }
            return;
        }

        if (!presets.loading.isEmpty()) {
            presets.delayed = true;
        } else {
            runProgram();
        }
    }

    void runProgram() {
        if (rp == null)
            rp = new RunProgram(pr, handler);

        s = eState.START;
        rp.run();

        start();

        onStart();
    }

    public void setFlatVcVol(float v) {
        mSoundVcVolume = Sound.log1(v);
        resetAllVolumes();
    }

    public void setFlatBgVol(float v) {
        mSoundBGVolume = Sound.log1(v);
        resetAllVolumes();
    }

    public void softEnd() {
        boolean alive = softStopVoices() || softStopBackground();
        if (!alive) {
            onEnd();
            return;
        }
        Log.d(TAG, "softEnd");
        handler.removeCallbacks(softEnd);
        handler.postDelayed(softEnd, 100);
    }

    public boolean isEnd() {
        return s == eState.END;
    }

    public void onStart() {
        Log.d(TAG, "onStart");
    }

    public void onPause() {
        Log.d(TAG, "onPause");
    }

    public void onEnd() {
        Log.d(TAG, "onEnd");
    }
}
